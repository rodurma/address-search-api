import nock from 'nock';
import environmentConfig from '../../src/config/environment.config';

export function getAddressSuccessfully(zipcode: string): void {
  nock(environmentConfig.apis.brasilapi.baseUrl)
    .get(`/cep/v1/${zipcode}`)
    .reply(200, {
      cep: zipcode,
      state: 'SP',
      city: 'Ribeirão Preto',
      neighborhood: 'Iguatemi',
      street: 'Rua Alfredo Benzoni',
      service: 'viacep',
    });
}

export function getAddressFromBrasilApiWithServerError(zipcode: string): void {
  nock(environmentConfig.apis.brasilapi.baseUrl)
    .get(`/cep/v1/${zipcode}`)
    .reply(503);
}

export function getAddressFromBrasilApiWithTimeoutError(zipcode: string): void {
  nock(environmentConfig.apis.brasilapi.baseUrl)
    .get(`/cep/v1/${zipcode}`)
    .reply(408);
}

export function getAddressFromBrasilApiWithNotFoundError(
  zipcode: string,
): void {
  nock(environmentConfig.apis.brasilapi.baseUrl)
    .get(`/cep/v1/${zipcode}`)
    .reply(404);
}
