import ZipcodeService from '../../src/modules/address/services/zipcode/zipcode.service';

describe('[Unit] ZipcodeService', () => {
  it('should return a zipcode list by one zipcode', () => {
    const zipcodeService = new ZipcodeService();

    const expectedZipcodeList: string[] = [
      '14091520',
      '14091500',
      '14091000',
      '14090000',
      '14000000',
      '10000000',
    ];

    const zipcodeList: string[] = zipcodeService.getList('14091520');

    expect(zipcodeList).toEqual(expectedZipcodeList);
  });
});
