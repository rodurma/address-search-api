import { container } from 'tsyringe';
import BrasilApiService from '../../src/modules/address/services/brasilapi/brasilapi.service';
import * as nocks from '../utils/nocks';

describe('[Unit] BrasilApiService', () => {
  it('should return an address by zipcode', async () => {
    const brasilApiService = container.resolve(BrasilApiService);

    const expectAddress = {
      zipcode: '14091520',
      state: 'SP',
      city: 'Ribeirão Preto',
      district: 'Iguatemi',
      place: 'Rua Alfredo Benzoni',
    };

    nocks.getAddressSuccessfully('14091520');

    const address = await brasilApiService.getAddresByZipcode('14091520');

    expect(address).toEqual(expectAddress);
  });
});
