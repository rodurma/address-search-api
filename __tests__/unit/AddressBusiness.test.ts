import { container } from 'tsyringe';
import AddressBussiness from '../../src/modules/address/address.business';
import * as nocks from '../utils/nocks';

describe('[Unit] AddressBussiness', () => {
  let addressBussiness: AddressBussiness;

  beforeAll(() => {
    addressBussiness = container.resolve(AddressBussiness);
  });

  it('should Brasil API return server error', async () => {
    nocks.getAddressFromBrasilApiWithServerError('14091520');

    try {
      await addressBussiness.getAddresByZipcode('14091520');
    } catch (error) {
      expect(error.message).toBe('Service Unavailable');
    }
  });

  it('should Brasil API timeout', async () => {
    nocks.getAddressFromBrasilApiWithTimeoutError('14091520');

    try {
      await addressBussiness.getAddresByZipcode('14091520');
    } catch (error) {
      expect(error.message).toBe('Request Time-out');
    }
  });

  it('should Brasil API returns not found errors', async () => {
    nocks.getAddressFromBrasilApiWithNotFoundError('10000001');
    nocks.getAddressFromBrasilApiWithNotFoundError('10000000');

    try {
      await addressBussiness.getAddresByZipcode('10000001');
    } catch (error) {
      expect(error.message).toBe('Endereço não encontrado');
    }
  });
});
