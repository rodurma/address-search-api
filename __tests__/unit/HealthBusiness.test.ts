import { container } from 'tsyringe';
import HealthBusiness from '../../src/modules/health/health.business';

describe('[Unit] HealthBusiness', () => {
  let healthBusiness: HealthBusiness;

  beforeAll(() => {
    healthBusiness = container.resolve(HealthBusiness);
  });

  it('should return PingResponse successfully', async () => {
    const health = healthBusiness.ping();

    const expectHealth = {
      status: 'pong',
    };

    expect(health).toEqual(expectHealth);
  });
});
