import { Server } from '@hapi/hapi';
import JWT from 'jsonwebtoken';
import App from '../../src/app/app';
import environment from '../../src/config/environment.config';
import route from '../../src/utils/helpers';
import * as nocks from '../utils/nocks';

let server: Server;

const jwtToken = JWT.sign({}, environment.app.auth.tokenSecret);

const authHeader = {
  authorization: `Bearer ${jwtToken}`,
};

describe('[Feature] AddressController', () => {
  beforeAll(async done => {
    server = await new App().configure();
    await server.initialize();
    done();
  }, 30000);

  afterAll(async done => {
    if (server) {
      await server.stop();
    }
    done();
  });

  it('should find address by zicode successfully', async () => {
    nocks.getAddressSuccessfully('14096180');

    const response = await server.inject({
      method: 'GET',
      url: route('/address/14096180'),
      headers: authHeader,
    });

    expect(response.statusCode).toBe(200);
  });

  it('should return error for invalid zipcode', async () => {
    const response = await server.inject({
      method: 'GET',
      url: route('/address/140961'),
      headers: authHeader,
    });

    expect(response.statusCode).toBe(400);
  });
});
