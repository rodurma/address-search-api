import { Server } from '@hapi/hapi';
import App from '../../src/app/app';
import route from '../../src/utils/helpers';

let server: Server;

describe('[Feature] HealthController', () => {
  beforeAll(async done => {
    server = await new App().configure();
    await server.initialize();
    done();
  }, 30000);

  afterAll(async done => {
    if (server) {
      await server.stop();
    }
    done();
  });

  it('should test health check', async () => {
    const response = await server.inject({
      method: 'GET',
      url: route('/health/ping'),
    });

    expect(response.statusCode).toBe(200);
  });
});
