import { Server } from '@hapi/hapi';
import App from '../../src/app/app';
import route from '../../src/utils/helpers';

let server: Server;

describe('[Feature] Default Routes', () => {
  beforeAll(async done => {
    server = await new App().configure();
    await server.initialize();
    done();
  }, 30000);

  afterAll(async done => {
    if (server) {
      await server.stop();
    }
    done();
  });

  it('should find address by zicode successfully2', async () => {
    const response = await server.inject({
      method: 'GET',
      url: route('/'),
    });

    expect(response.statusCode).toBe(302);
  });
});
