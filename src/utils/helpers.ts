import environment from '../config/environment.config';

const route = (path: string): string => {
  const prefix = environment.app.routes.prefix || '';

  return prefix.concat(path);
};

export default route;
