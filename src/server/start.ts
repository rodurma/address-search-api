import '@abraham/reflection';
import dotenv from 'dotenv';
import App from '../app/app';
import '../config/container.config';

dotenv.config();

(async () => {
  const app = await new App().configure();

  process.on('unhandledRejection', err => {
    console.log(err);
    process.exit(1);
  });

  await app.start();
})();
