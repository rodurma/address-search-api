import { HealthBusinessInterface, PingResponse } from './health.interface';

export default class HealthBusiness implements HealthBusinessInterface {
  /**
   * Send pong status
   * @returns {PingResponse}
   */
  public ping(): PingResponse {
    return {
      status: 'pong',
    };
  }
}
