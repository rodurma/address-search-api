import { Server } from '@hapi/hapi';
import { container } from 'tsyringe';
import HealthController from './health.controller';
import { healthDocsDefinition } from './health.docs';

/**
 * Define Health module routes
 */
export default (server: Server): void => {
  const controller = container.resolve(HealthController);

  server.bind(controller);

  server.route([
    {
      method: 'GET',
      path: '/health/ping',
      options: {
        handler: controller.ping,
        auth: false,
        description: 'Ping/Pong',
        tags: ['api'],
        plugins: { ...healthDocsDefinition },
      },
    },
  ]);
};
