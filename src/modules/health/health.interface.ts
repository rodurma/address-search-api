export interface HealthControllerInterface {
  ping(): PingResponse;
}

export interface PingResponse {
  status: string;
}

export interface HealthBusinessInterface {
  ping(): PingResponse;
}
