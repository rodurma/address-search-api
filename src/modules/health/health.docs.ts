export const healthDocsDefinition = {
  'hapi-swagger': {
    responses: {
      '200': {
        description: 'Ping/Pong',
        schema: {
          type: 'object',
          properties: {
            status: {
              type: 'string',
              example: 'pong',
            },
          },
        },
      },
    },
  },
};
