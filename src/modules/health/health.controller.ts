import { inject, injectable } from 'tsyringe';
import {
  HealthBusinessInterface,
  HealthControllerInterface,
  PingResponse,
} from './health.interface';

@injectable()
export default class HealthController implements HealthControllerInterface {
  constructor(
    @inject('HealthBusinessInterface')
    private healthBusiness: HealthBusinessInterface,
  ) {}

  /**
   * Send pong status
   * @returns {PingResponse}
   */
  public ping(): PingResponse {
    return this.healthBusiness.ping();
  }
}
