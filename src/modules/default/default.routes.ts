import { Server, Request, ResponseToolkit } from '@hapi/hapi';
import route from '../../utils/helpers';

/**
 * Define Default module routes
 */
export default (server: Server): void => {
  server.route([
    {
      method: 'GET',
      path: '/',
      options: {
        handler: async (_request: Request, h: ResponseToolkit) => {
          return h.response().redirect(route('/docs'));
        },
        description: 'Redirect to /docs',
        auth: false,
      },
    },
  ]);
};
