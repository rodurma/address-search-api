import { Request } from '@hapi/hapi';
import { inject, injectable } from 'tsyringe';
import {
  AddressBusinessInterface,
  AddressControllerInterface,
  AddressResponse,
} from './address.interface';

@injectable()
export default class AddressController implements AddressControllerInterface {
  constructor(
    @inject('AddressBussinessInterface')
    private addressBussiness: AddressBusinessInterface,
  ) {}

  /**
   *
   * @param {Request} request
   * @returns {AddressResponse}
   */
  public async findAddresByZipcode({
    params: { zipcode },
  }: Request): Promise<AddressResponse> {
    return this.addressBussiness.getAddresByZipcode(zipcode);
  }
}
