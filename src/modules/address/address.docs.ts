export const addressDocsDefinition = {
  'hapi-swagger': {
    responses: {
      '200': {
        description: 'Address object',
        schema: {
          type: 'object',
          properties: {
            zipcode: {
              type: 'string',
              example: '14091520',
            },
            state: {
              type: 'string',
              example: 'SP',
            },
            city: {
              type: 'string',
              example: 'Ribeirão Preto',
            },
            district: {
              type: 'string',
              example: 'Ribeirânia',
            },
            place: {
              type: 'string',
              example: 'Avenida Leão XIII',
            },
          },
        },
      },
      '404': {
        description: 'Endereço não encontrado',
        schema: {
          type: 'object',
          properties: {
            statusCode: {
              type: 'number',
              example: 404,
            },
            error: {
              type: 'string',
              example: 'Not Found',
            },
            message: {
              type: 'string',
              example: 'Endereço não encontrado',
            },
          },
        },
      },
      '400': {
        description: 'Cep inválido',
        schema: {
          type: 'object',
          properties: {
            statusCode: {
              type: 'number',
              example: 400,
            },
            error: {
              type: 'string',
              example: 'Bad Request',
            },
            message: {
              type: 'string',
              example: 'Cep inválido',
            },
          },
        },
      },
      '401': {
        description: 'Unauthorized',
        schema: {
          type: 'object',
          properties: {
            statusCode: {
              type: 'number',
              example: 401,
            },
            error: {
              type: 'string',
              example: 'Unauthorized',
            },
            message: {
              type: 'string',
              example: 'Missing authentication',
            },
            attributes: {
              type: 'object',
              properties: {
                error: {
                  type: 'string',
                  example: 'Invalid token',
                },
              },
            },
          },
        },
      },
    },
    payloadType: 'form',
  },
};
