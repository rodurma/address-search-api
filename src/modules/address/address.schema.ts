import Joi from 'joi';
import { BAD_REQUEST } from 'http-status';
import { Boom } from '@hapi/boom';

const findAddresByZipcodeSchema = {
  params: Joi.object({
    zipcode: Joi.string()
      .required()
      .regex(new RegExp(/^(?!00000000)[0-9]{8}$/)),
  }),
  failAction: (): Boom => {
    throw new Boom('Cep inválido', {
      statusCode: BAD_REQUEST,
    });
  },
};

export default findAddresByZipcodeSchema;
