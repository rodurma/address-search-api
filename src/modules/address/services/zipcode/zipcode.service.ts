import ZipcodeServiceInterface from './zipcode.interface';

export default class ZipcodeService implements ZipcodeServiceInterface {
  static INVALID_ZIPCODE = '00000000';

  /**
   * Get the next zip code
   * @param {string} zipcode
   * @param {number} length
   * @returns {string}
   */
  public getCode(zipcode: string, length: number): string {
    if (length >= zipcode.length) {
      return zipcode;
    }

    return this.replaceAt(zipcode, length, '0');
  }

  /**
   * Generates a list of possible unique ceps
   * @param {string} zipcode
   * @returns {string[]} Zipcode list
   */
  public getList(zipcode: string): string[] {
    const zipcodeList: string[] = [];
    let currentZipcode: string = zipcode;
    let index: number = zipcode.length;

    while (
      this.getCode(currentZipcode, index) !== ZipcodeService.INVALID_ZIPCODE
    ) {
      currentZipcode = this.getCode(currentZipcode, index);

      zipcodeList.push(currentZipcode);

      while (currentZipcode[index - 1] === '0') {
        index -= 1;
      }

      index -= 1;
    }

    return zipcodeList;
  }

  /**
   * Replaces a char at a specified position
   * @param {string} zipcode
   * @param {number} index Replacemento position
   * @param {string} replacement Character that will be used in the replacement
   * @returns
   */
  public replaceAt(
    zipcode: string,
    index: number,
    replacement: string,
  ): string {
    return (
      zipcode.substr(0, index) +
      replacement +
      zipcode.substr(index + replacement.length)
    );
  }
}
