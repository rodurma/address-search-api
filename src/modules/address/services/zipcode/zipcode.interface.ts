export default interface ZipcodeServiceInterface {
  getCode(zipcode: string, length: number): string;

  getList(zipcode: string): string[];

  replaceAt(zipcode: string, index: number, replacement: string): string;
}
