import { AddressResponse } from '../../address.interface';

export default interface BrasilApiServiceInterface {
  getAddresByZipcode(zipcode: string): Promise<AddressResponse>;
}

export interface BrasilApiResponse {
  cep: string;
  state: string;
  city: string;
  neighborhood: string;
  street: string;
  service: string;
}
