import { AxiosInstance } from 'axios';
import { inject, injectable } from 'tsyringe';
import * as httpStatus from 'http-status';
import { Boom } from '@hapi/boom';
import { AddressResponse } from '../../address.interface';
import BrasilApiServiceInterface, {
  BrasilApiResponse,
} from './brasilapi.interface';
import environment from '../../../../config/environment.config';

@injectable()
export default class BrasilApiService implements BrasilApiServiceInterface {
  constructor(@inject('BrasilApiAxios') private axios: AxiosInstance) {
    this.axios.defaults.baseURL = environment.apis.brasilapi.baseUrl;
    this.axios.defaults.timeout = environment.apis.brasilapi.timeout;
  }

  /**
   * Get address by zipcode
   * @param {string} zipcode
   * @returns {Promise<AddressResponse>}
   */
  public async getAddresByZipcode(zipcode: string): Promise<AddressResponse> {
    const { data: address } = await this.axios.get<BrasilApiResponse>(
      `/cep/v1/${zipcode}`,
    );

    return {
      zipcode: address.cep,
      state: address.state,
      city: address.city,
      district: address.neighborhood,
      place: address.street,
    };
  }
}
