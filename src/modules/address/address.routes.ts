import { Server } from '@hapi/hapi';
import { container } from 'tsyringe';
import environment from '../../config/environment.config';
import AddressController from './address.controller';
import { addressDocsDefinition } from './address.docs';
import findAddresByZipcodeSchema from './address.schema';

/**
 * Define Address module routes
 */
export default (server: Server): void => {
  const controller = container.resolve(AddressController);

  server.bind(controller);

  server.route([
    {
      method: 'GET',
      path: '/address/{zipcode}',
      options: {
        handler: controller.findAddresByZipcode,
        auth: {
          mode: 'required',
        },
        description: 'Get address',
        notes: 'Return an address by zipcode',
        tags: ['api'],
        plugins: { ...addressDocsDefinition },
        validate: findAddresByZipcodeSchema,
        ...(environment.app.routes.cache.header.active && {
          cache: {
            expiresIn: environment.app.routes.cache.header.ttl,
            privacy: 'private',
          },
        }),
      },
    },
  ]);
};
