import { Request } from '@hapi/hapi';

export interface AddressControllerInterface {
  findAddresByZipcode(request: Request): Promise<AddressResponse>;
}

export interface AddressResponse {
  zipcode: string;
  state: string;
  city: string;
  district: string;
  place: string;
}

export interface AddressBusinessInterface {
  getAddresByZipcode(zipcode: string): Promise<AddressResponse>;
}
