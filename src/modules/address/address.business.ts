import { Boom } from '@hapi/boom';
import { inject, injectable } from 'tsyringe';
import { NOT_FOUND } from 'http-status';
import { AddressBusinessInterface, AddressResponse } from './address.interface';
import ZipcodeServiceInterface from './services/zipcode/zipcode.interface';
import BrasilApiServiceInterface from './services/brasilapi/brasilapi.interface';

@injectable()
export default class AddressBusiness implements AddressBusinessInterface {
  constructor(
    @inject('ZipcodeServiceInterface')
    private zipcodeService: ZipcodeServiceInterface,
    @inject('BrasilApiServiceInterface')
    private brasilApi: BrasilApiServiceInterface,
  ) {}

  /**
   * Get an address by zipcode
   * @param {string} zipcode
   * @returns {Promise<AddressResponse>}
   */
  public async getAddresByZipcode(zipcode: string): Promise<AddressResponse> {
    const zipCodeList: string[] = this.zipcodeService.getList(zipcode);

    let index = 0;

    while (index < zipCodeList.length) {
      try {
        const address = await this.brasilApi.getAddresByZipcode(
          zipCodeList[index],
        );

        return address;
      } catch (error) {
        if ('isBoom' in error) {
          throw error;
        }

        index += 1;
      }
    }

    throw new Boom('Endereço não encontrado', {
      statusCode: NOT_FOUND,
    });
  }
}
