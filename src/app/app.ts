import { Server, Request } from '@hapi/hapi';
import klawSync from 'klaw-sync';
import AppInterface from './app.interface';
import environment from '../config/environment.config';
import { plugins } from '../config/plugins.config';
import { RequestLogger, RequestLoggerMapper } from '../plugins/logger.plugin';

class App implements AppInterface {
  protected server: Server;

  /**
   * Initialize hapi server instance
   */
  public init(): void {
    this.server = new Server({
      port: environment.app.port,
      host: environment.app.host,
      routes: {
        timeout: {
          server: environment.app.routes.timeout,
          socket: environment.app.routes.timeout + 1000,
        },
        cors: environment.app.routes.cors,
      },
      router: {
        stripTrailingSlash: true,
      },
    });
  }

  /**
   * Define routes
   * @returns {void}
   */
  public async routes(): Promise<void> {
    this.server.realm.modifiers.route.prefix =
      environment.app.routes.prefix || '';

    (klawSync(environment.app.routes.routesBaseDir) || [])
      .filter(item => /([a-zA-Z-_]+)\.(routes)\.[tj]s$/.test(item.path))
      .forEach(route => {
        require(route.path).default(this.server);
      });
  }

  /**
   * Define plugins
   * @returns {void}
   */
  public async plugins(): Promise<void> {
    await this.server.register(plugins);

    if (environment.app.logger.active) {
      const logger = RequestLogger();

      this.server.events.on('response', (request: Request) => {
        if (!/\/docs(.*)/.test(request.url.pathname)) {
          logger.info(RequestLoggerMapper(request));
        }
      });
    }
  }

  /**
   * Define auth
   * @returns void
   */
  public auth(): void {
    const validate = async () => {
      return { isValid: true };
    };

    this.server.auth.strategy('jwt', 'jwt', {
      key: environment.app.auth.tokenSecret,
      validate,
      verifyOptions: {
        algorithms: ['HS256'],
        ignoreExpiration: true,
      },
    });

    this.server.auth.default('jwt');
  }

  public async configure(): Promise<Server> {
    this.init();
    await this.plugins();
    this.auth();
    await this.routes();

    return this.server;
  }
}

export default App;
