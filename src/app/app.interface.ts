import { Server } from '@hapi/hapi';

export default interface AppInterface {
  /**
   * Initialize hapi server instance
   * @returns {void}
   */
  init(): void;
  /**
   * Define routes
   * @returns {Promise<void>}
   */
  routes(): Promise<void>;

  /**
   * Define plugins
   * @returns {Promise<void>}
   */
  plugins(): Promise<void>;

  /**
   * Define auth
   * @returns {void}
   */
  auth(): void;

  /**
   * Configure server
   * @returns {Promise<Server>}
   */
  configure(): Promise<Server>;
}
