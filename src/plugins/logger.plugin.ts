import { Boom } from '@hapi/boom';
import { ResponseObject } from '@hapi/hapi';
import Logger, * as bunyan from 'bunyan';
import Elasticsearch from 'bunyan-elasticsearch';
import environment from '../config/environment.config';

export const RequestLogger = (): Logger => {
  const esStream = new Elasticsearch({
    indexPattern: `[${environment.app.logger.index}]`,
    type: 'logs',
    host: environment.app.logger.host,
  });

  esStream.on('error', err => {
    console.log('Elasticsearch Stream Error:', err.stack);
  });

  const logger = bunyan.createLogger({
    name: environment.app.name,
    streams: [{ stream: esStream }],
    serializers: bunyan.stdSerializers,
  });

  return logger;
};

export const RequestLoggerMapper = (
  request: ResponseObject | Boom<any> | any,
): any => {
  delete request.headers.authorization;

  return {
    method: request.method,
    response_time: request.response.headers['x-response-time'] || null,
    request_headers: JSON.stringify(request.headers),
    uri: request.url.pathname,
    path: request.url.search,
    url: `${request.headers.host}`,
    protocol: request.url.protocol,
    host: request.headers.host,
    hostAddress: request?.info?.host,
    remoteAddress: request?.info?.remoteAddress,
    response_headers: JSON.stringify(request.response.headers),
    response_body: JSON.stringify(request.response.source),
    statusCode: request.response.statusCode,
  };
};
