import Inert from '@hapi/inert';
import Vision from '@hapi/vision';
import * as Hapi from '@hapi/hapi';
import * as HapiSwagger from 'hapi-swagger';
import * as Blipp from 'blipp';
import * as ResponseTime from 'hapi-response-time';
import * as JWT from 'hapi-auth-jwt2';
import * as StatusMonitor from 'hapijs-status-monitor';
import route from '../utils/helpers';

const pack = require('../../package.json');

export const plugins: Array<Hapi.ServerRegisterPluginObject<any>> = [
  {
    plugin: Inert,
  },
  {
    plugin: Vision,
  },
  {
    plugin: HapiSwagger,
    options: {
      basePath: route('/'),
      documentationPath: route('/docs'),
      info: {
        title: pack.displayName,
        description: pack.description,
        version: pack.version,
        contact: {
          name: pack.author.name,
          email: pack.author.email,
        },
      },
      sortEndpoints: 'ordered',
      grouping: 'path',
      securityDefinitions: {
        jwt: {
          type: 'apiKey',
          name: 'Authorization',
          in: 'header',
        },
      },
      security: [{ jwt: [] }],
    },
  },
  {
    plugin: Blipp,
  },
  {
    plugin: ResponseTime,
  },
  {
    plugin: JWT,
  },
  {
    plugin: StatusMonitor,
    options: {
      title: 'Address API Status Monitor',
      routeConfig: {
        auth: false,
      },
    },
  },
];
