import dotenv from 'dotenv';
import path from 'path';

const pack = require('../../package.json');

(() => {
  process.env.NODE_ENV = process.env.NODE_ENV || 'development';
  const { NODE_ENV } = process.env;
  const envPath = path.join(__dirname, `/../../${`/env/.env-${NODE_ENV}`}`);
  dotenv.config({
    path: envPath,
  });
})();

export default {
  env: process.env.NODE_ENV,
  app: {
    name: pack.displayName,
    host: process.env.APP_SERVER_HOST,
    port: process.env.APP_SERVER_PORT,
    routes: {
      routesBaseDir: path.normalize(path.join(__dirname, '../modules')),
      prefix: process.env.APP_ROUTE_PREFIX,
      timeout: Number(process.env.APP_SERVER_TIMEOUT),
      cors: process.env.APP_SERVER_CORS === 'true',
      cache: {
        header: {
          active: process.env.CACHE_HEADER_ACTIVE === 'true',
          ttl: Number(process.env.CACHE_HEADER_TTL) * 1000,
        },
      },
    },
    auth: {
      tokenSecret: process.env.APP_TOKEN_SECRET,
    },
    logger: {
      active: process.env.LOGGER_ACTIVE === 'true',
      index: process.env.LOGGER_INDEX,
      host: process.env.LOGGER_HOST,
    },
  },
  apis: {
    brasilapi: {
      baseUrl: process.env.BRASIL_API_URL,
      timeout: Number(process.env.BRASIL_API_TIMEOUT),
    },
  },
};
