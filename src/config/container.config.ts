import { Boom } from '@hapi/boom';
import axios, { AxiosInstance } from 'axios';
import httpStatus from 'http-status';
import { container } from 'tsyringe';
import AddressBussiness from '../modules/address/address.business';
import { AddressBusinessInterface } from '../modules/address/address.interface';
import BrasilApiServiceInterface from '../modules/address/services/brasilapi/brasilapi.interface';
import BrasilApiService from '../modules/address/services/brasilapi/brasilapi.service';
import ZipcodeServiceInterface from '../modules/address/services/zipcode/zipcode.interface';
import ZipcodeService from '../modules/address/services/zipcode/zipcode.service';
import HealthBusiness from '../modules/health/health.business';
import { HealthBusinessInterface } from '../modules/health/health.interface';

container.register<AddressBusinessInterface>(
  'AddressBussinessInterface',
  AddressBussiness,
);

container.register<ZipcodeServiceInterface>(
  'ZipcodeServiceInterface',
  ZipcodeService,
);

container.register<AxiosInstance>('BrasilApiAxios', {
  useFactory: () => {
    axios.interceptors.response.use(
      config => config,
      error => {
        const {
          response: { status },
        } = error;
        if (error.code === 'ECONNABORTED' || status === 408) {
          throw new Boom(httpStatus[408], {
            statusCode: httpStatus.REQUEST_TIMEOUT,
          });
        } else if (status >= 500) {
          throw new Boom(error.response.statusText, {
            statusCode: error.response.status,
          });
        }

        return Promise.reject(error);
      },
    );

    return axios;
  },
});

container.register<BrasilApiServiceInterface>(
  'BrasilApiServiceInterface',
  BrasilApiService,
);

container.register<HealthBusinessInterface>(
  'HealthBusinessInterface',
  HealthBusiness,
);
