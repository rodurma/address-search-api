# Address API

Address API é uma aplicação responsável por buscar dados de um endereço através do seu CEP.

# Sumário

- [Pré-requisitos](#Pré-requisitos)
- [Configuração](#Configuração)
- [Testes](#Testes)
- [Executar](#Executar)
- [Arquitetura](#Arquitetura)
- [Qualidade](#Qualidade)
- [Monitoramento](#Monitoramento)
- [Documentação](#Documentação)
- [CI](#CI)
- [Pergunta](#Pergunta)

# Pré-requisitos

- [Node](https://nodejs.org/en/)
- [Docker compose](https://docs.docker.com/compose/install/)

# Configuração

Faça o clone do projeto:

```
git clone git@gitlab.com:rodurma/address-search-api.git
cd address-search-api
```

Instale as dependências do projeto:

```
npm install
```

Configure as variáveis de ambiente de acordo com o ambiente:

| Nome | Descrição | Valor Padrão | Obrigatório |
| -- | -- | -- | -- |
| NODE_ENV | Ambiente | development | Sim |
| APP_SERVER_PORT | Porta do servidor | 3333 | Sim |
| APP_SERVER_HOST | Host do servidor | localhost | Sim |
| APP_SERVER_CORS | Cors ativo | true | Sim |
| APP_SERVER_TIMEOUT | Timeout | 10000 | Sim |
| APP_ROUTE_PREFIX | Prefixo das rotas | /api | Sim |
| APP_TOKEN_SECRET | Secret key JWT |  | Sim |
| BRASIL_API_URL | URL base | https://brasilapi.com.br/api | Sim |
| BRASIL_API_TIMEOUT | Timeout para requisições externas | Sim |
| LOGGER_ACTIVE | Log ElasticSearch | true | Sim |
| LOGGER_INDEX | Índice log ElasticSearch | address-api | Sim |
| LOGGER_HOST | Host ElasticSearch | localhost:9200 | Sim |
| CACHE_HEADER_ACTIVE | Cache headers ativos | true | Sim |
| CACHE_HEADER_TTL | Expiração headers ativos | 86400 | Sim |

Crie um token e secret:

```
npm run token {SEU SECRET}
```

Troque `{SEU SECRET}` por um hash de caracteres aleatórios e adicione o valor que você definiu no `{SEU SECRET}` na variável de ambiente `APP_TOKEN_SECRET`

Para iniciar localmente você pode alterar o arquivo `env/.env-development`

# Testes

Para executar os testes do projeto, execute o comando abaixo:

```
npm run test
```

<p align="center">
  <img src="./repo/coverage.png" alt="Elasticsearch e Kibana" width="1000">
</p>

# Executar

Primeiro execute o comando para inicializar os containers docker:

```
docker-compose up -d
```

Para iniciar a aplicação execute o comando abaixo:

```
npm run start:dev
```

# Arquitetura

A solução proposta foi o desenvolvimento de uma aplicação simples para a busca de endereço através do CEP. A API externa utilizada foi a [Brasil API](https://brasilapi.com.br/docs#tag/CEP) foi a mesma faz o roteamente de várias fontes onde os dados de um endereço podem ser buscados. A fonte de dados que devolver primeiro é retornada.

A ideia foi não usar recursividade mas sim foi criada um método que cria todas os ceps possíveis sem repetição para garantir que a aplicação não faça requisições na Brasil API de forma desnecessária.

Nesta aplicação foi utilizado o Node.js com TypeScript pela sua simplicidade e robustez ideal para aplicação que irão receber alta demanda de requisições. Também pelo fato do JavaScript ser tão popular e garantir a possibilidade de ser usado tanto no backend como no frontend, além de ser possível criar aplicações mobile e desktop.

# Qualidade

**TS Lint**: Para garantir a qualidade do código TypeScript.

**Prettier**: Para padronização de códigos.

# Monitoramento

Foi utilizado o Elasticsearch e Kibana para monitoramento e logs da aplicação

Ao executar o `docker-compose` acesse o serviço [Kibana](http://localhost:5601/app/kibana#/discover?_g=()) para ver os logs.

<p align="center">
  <img src="./repo/elasticsearch-log.png" alt="Elasticsearch e Kibana" width="1000">
</p>

# Documentação

Para documentar a API foi utilizado o Swagger.

Após inicializar a aplicação acesse http://localhost:3333/api/docs

# CI

Foi utilizado os recursos de pipelines do Gitlab para o processo de integração contínua.

https://gitlab.com/rodurma/address-search-api/-/pipelines

# Pergunta

> Quando você digita a URL de um site (http://www.netshoes.com.br) no browser e pressiona enter, explique da forma que preferir, o que ocorre nesse processo do protocolo HTTP entre o Client e o Server.

Quando digitamos um endereço no navegador e pressionamos enter, o navegador, através do protocolo HTTP, inicia uma comunicação com um servidor. Existem várias tecnologias envolvidas para que isso aconteça, considerando de forma resumida:

- Consulta em servidores DNS para tradução de nomes para IP
- Envio de fato da requisição para um servidor
- Servidor recebe a solicitação
- Verifica se possue o que foi solicitado
- Envia o que foi solicitado ou responde com um erro (404 por exemplo)

Este servidor pode ser o servidor em si ou um load balancer que irá distribuir a carga entre vários outros servidores o que garante alta disponibilidade.

Quando essa comunicação é estabelecida, o servidor passa a servir os dados do site. Podem acontecer não só uma mas várias requisições para um ou até mais servidores já que cada elemento do site gera uma requisição HTTP (imagens, css, js, etc).

O navegador após receber os dados, começa a renderizar o site no navegador.
